insert into operation (id, label, amount, operation_date, type, account_id) values (1, 'Retrait distributeur', 20.0, '2020-04-14', 'DEBIT', 1);
insert into operation (id, label, amount, operation_date, type, account_id) values (2, 'Paiement Cdiscount', 71.8, '2020-04-13', 'DEBIT', 1);
insert into operation (id, label, amount, operation_date, type, account_id) values (3, 'Virement', 123.6, '2020-04-12', 'CREDIT', 2);
insert into operation (id, label, amount, operation_date, type, account_id) values (4, 'Virement', 100.0, '2020-04-12', 'CREDIT', 3);