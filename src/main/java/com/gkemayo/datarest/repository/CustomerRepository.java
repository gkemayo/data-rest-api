package com.gkemayo.datarest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.gkemayo.datarest.entity.AccountType;
import com.gkemayo.datarest.entity.Customer;

@RepositoryRestResource(collectionResourceRel = "customerList", collectionResourceDescription = @Description("List of Customers"),
						itemResourceRel = "customer", itemResourceDescription = @Description("Customer"),
						path = "customerrepo")
//Accès au repository sur le path 'http://localhost:8080/api/customerrepo/'
//La liste des accounts renvoyés par l'hypermedia json de ce path est renommée en 'customerList'
//chaque élément de la liste est nommé 'customer'
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	@Query("SELECT c "
			+ "FROM Customer c "
			+ "INNER JOIN FETCH c.accounts a "
			+ "WHERE a.type = :accountType and a.balance > 0" )
	//Accessible sur le lien : http://localhost:8080/api/customerrepo/search/findPositiveAccountBalanceCustomers?accountType="XXX"
	List<Customer> findPositiveAccountBalanceCustomers(@Param("accountType") AccountType accountType);
	
	
	@Query(value = "SELECT id, identifier, full_name, birth_date "
			+ "FROM (SELECT c.*, sum(a.balance) sb FROM account a, customer c WHERE c.id = a.customer_id group by c.id having sb > 0) ", nativeQuery=true)
	//Accessible sur le lien : http://localhost:8080/api/customerrepo/search/findPositiveSumAccountBalancesCustomers
	List<Customer> findPositiveSumAccountBalancesCustomers();
	
}
