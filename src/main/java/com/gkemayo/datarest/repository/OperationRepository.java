package com.gkemayo.datarest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.gkemayo.datarest.entity.Operation;

@RepositoryRestResource(collectionResourceRel = "operationList", collectionResourceDescription = @Description("List of Operations"),
						itemResourceRel = "operation", itemResourceDescription = @Description("Operation"),
						path = "operationrepo")
//Accès au repository sur le path 'http://localhost:8080/api/operationrepo/'
//La liste des accounts renvoyés par l'hypermedia json de ce path est renommée en 'operationList'
//chaque élément de la liste est nommé 'operation'
public interface OperationRepository extends JpaRepository<Operation, Long> {
	
}
