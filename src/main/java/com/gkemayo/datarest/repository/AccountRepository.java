package com.gkemayo.datarest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.gkemayo.datarest.entity.Account;

@RepositoryRestResource(collectionResourceRel = "accountList", collectionResourceDescription = @Description("List of Accounts"),
						itemResourceRel = "account", itemResourceDescription = @Description("Account"),
						path = "accountrepo")
//Accès au repository sur le path 'http://localhost:8080/api/accountrepo/'
//La liste des accounts renvoyés par l'hypermedia json de ce path est renommée en 'accountList'
//chaque élément de la liste est nommé 'account'
public interface AccountRepository extends JpaRepository<Account, Long> {
	
}
