package com.gkemayo.datarest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.mapping.ExposureConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import org.springframework.http.HttpMethod;

import com.gkemayo.datarest.controller.CustomerRestController;
import com.gkemayo.datarest.entity.Customer;

@SpringBootApplication
public class DataRestApiApplication implements RepositoryRestConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(DataRestApiApplication.class, args);
	}
	
	/**
	 * Cette méthode implementée de l'interface RepositoryRestConfigurer permet de ne pas exposer les 
	 * méthodes HTTP PUT, POST, PATCH, DELETE de notre controller CustomerRestController. Nous en avons pas besoin dans notre exemple.
	 */
	@Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration repositoryRestConfiguration) {
		ExposureConfiguration config = repositoryRestConfiguration.getExposureConfiguration();
		config.forDomainType(CustomerRestController.class); 
		config.withCollectionExposure((metadata, httpMethods) -> httpMethods.disable(HttpMethod.PUT, HttpMethod.POST, HttpMethod.PATCH, HttpMethod.DELETE));
		config.withItemExposure((metadata, httpMethods) -> httpMethods.disable(HttpMethod.PUT, HttpMethod.POST, HttpMethod.PATCH, HttpMethod.DELETE));
		
//         config.getExposureConfiguration()
//                .forDomainType(CustomerRestController.class)
//                .withItemExposure((metdata, httpMethods) -> httpMethods.disable(HttpMethod.PUT, HttpMethod.POST, HttpMethod.PATCH, HttpMethod.DELETE));
    }
	
	/**
	 * Ajoute les liens http://localhost:8080/api/customer/positiveBalance?accountType=XXX 
	 * et http://localhost:8080/api/customer/positiveSumBalance dans le relation customerList 
	 * de Spring Data REST ainsi que de manière visuel dans son HAL explorer
	 * 
	 * @return
	 */
	@Bean
	public RepresentationModelProcessor<CollectionModel<EntityModel<Customer>>> customersPositiveBalanceProcessor() {

	   return new RepresentationModelProcessor<CollectionModel<EntityModel<Customer>>>() {

	     @Override
	     public CollectionModel<EntityModel<Customer>> process(CollectionModel<EntityModel<Customer>> resource) {
	    	 
	      resource.add(linkTo(CustomerRestController.class).slash("api").slash("customer").slash("positiveBalance").slash("?accountType=XXX").withRel("customersPositiveBalance"));
	      resource.add(linkTo(CustomerRestController.class).slash("api").slash("customer").slash("positiveSumBalance").withRel("customersPositiveSumBalance"));
	      return resource;
	     }
	   };
	}
		
//	@Bean
//	public RepositoryRestConfigurerAdapter configureXXX() {
//		
//		return new RepositoryRestConfigurerAdapter() {
//			@Override
//			  public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
//
//			    config.getCorsRegistry().addMapping("/person/**")
//			      .allowedOrigins("http://domain2.example")
//			      .allowedMethods("PUT", "DELETE")
//			      .allowedHeaders("header1", "header2", "header3")
//			      .exposedHeaders("header1", "header2")
//			      .allowCredentials(false).maxAge(3600);
//			  }
//		  };
//	}


}
