package com.gkemayo.datarest.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.gkemayo.datarest.entity.AccountType;
import com.gkemayo.datarest.entity.Customer;
import com.gkemayo.datarest.repository.CustomerRepository;

@RepositoryRestController
@BasePathAwareController
//en réalité, l'@RepositoryRestController ne sert à rien contrairement à ce que dit la doc Spring data rest. 
//C'est l'@BasePathAwareController qui est utile et qui injecte la base-path qui est ici '/api' 
public class CustomerRestController {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	/**
	 * Récupère tous les Customers dont la balance du compte identifier par 'accountType'
	 * est supérieur à zero 
	 * 
	 * @param accountType
	 * @return
	 */
	@GetMapping("/customer/positiveBalance")
	public ResponseEntity<CollectionModel<EntityModel<Customer>>> getPositiveAccountBalanceCustomers(@RequestParam("accountType") String accountType) {
		
		AccountType type = Arrays.asList(AccountType.values())
								 .stream().filter(e -> e.name().equals(accountType)).findAny().orElse(null);
		List<Customer> customers = customerRepository.findPositiveAccountBalanceCustomers(type);
		
		CollectionModel<EntityModel<Customer>> customersModel = CollectionModel.wrap(customers); 
		//customersModel.add(linkTo(methodOn(CustomerRestController.class).getPositiveAccountBalanceCustomers(accountType)).withRel("customersPositiveBalance"));
		
		return new ResponseEntity<CollectionModel<EntityModel<Customer>>>(customersModel, HttpStatus.OK);
	}
	
	/**
	 * Récupère tous les Customers dont la somme cumulée de l'ensemble de leurs comptes
	 * confondus est supérieur à zero 
	 * 
	 * @return
	 */
	@GetMapping("/customer/positiveSumBalance")
	public ResponseEntity<CollectionModel<EntityModel<Customer>>> getPositiveSumAccountBalancesCustomers() {
		
		List<Customer> customers = customerRepository.findPositiveSumAccountBalancesCustomers();
		
		CollectionModel<EntityModel<Customer>> customersModel = CollectionModel.wrap(customers);
		//customersModel.add(linkTo(methodOn(CustomerRestController.class).getPositiveSumAccountBalancesCustomers()).withRel("customersPositiveSumBalance"));
		
		return new ResponseEntity<CollectionModel<EntityModel<Customer>>>(customersModel, HttpStatus.OK);
	}
	
}
