package com.gkemayo.datarest.controller.client;

import java.net.URI;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gkemayo.datarest.entity.Account;
import com.gkemayo.datarest.entity.Customer;
import com.gkemayo.datarest.entity.Operation;

@RestController
@RequestMapping("/client")
public class SpringDataRestClient {
	
	@GetMapping("/customers")
	public ResponseEntity<CollectionModel<EntityModel<Customer>>> customers() {
		Traverson traverson = new Traverson(URI.create("http://localhost:8080/api"), MediaTypes.HAL_JSON);
		
		ParameterizedTypeReference<CollectionModel<EntityModel<Customer>>> customersType = new ParameterizedTypeReference<CollectionModel<EntityModel<Customer>>>() {};
		CollectionModel<EntityModel<Customer>> customers = traverson.follow("customerList").toObject(customersType);
		
		return new ResponseEntity<CollectionModel<EntityModel<Customer>>>(customers, HttpStatus.OK);
	}
	
	@GetMapping("/accounts")
	public ResponseEntity<CollectionModel<EntityModel<Account>>> accounts() {
		Traverson traverson = new Traverson(URI.create("http://localhost:8080/api"), MediaTypes.HAL_JSON);
		
		ParameterizedTypeReference<CollectionModel<EntityModel<Account>>> accountType = new ParameterizedTypeReference<CollectionModel<EntityModel<Account>>>() {};
		CollectionModel<EntityModel<Account>> accounts = traverson.follow("accountList").toObject(accountType);
		
		return new ResponseEntity<CollectionModel<EntityModel<Account>>>(accounts, HttpStatus.OK);
	}
	
	@GetMapping("/operations")
	public ResponseEntity<CollectionModel<EntityModel<Operation>>> operations() {
		Traverson traverson = new Traverson(URI.create("http://localhost:8080/api"), MediaTypes.HAL_JSON);
		
		ParameterizedTypeReference<CollectionModel<EntityModel<Operation>>> OperationType = new ParameterizedTypeReference<CollectionModel<EntityModel<Operation>>>() {};
		CollectionModel<EntityModel<Operation>> operationsModel = traverson.follow("operationList").toObject(OperationType);
		
		return new ResponseEntity<CollectionModel<EntityModel<Operation>>>(operationsModel, HttpStatus.OK);
	}

}
