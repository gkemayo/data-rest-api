package com.gkemayo.datarest.entity;

public enum OperationType {
	
	DEBIT, CREDIT;
	
}
