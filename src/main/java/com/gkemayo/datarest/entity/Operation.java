package com.gkemayo.datarest.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
//@RestResource(rel = "coperationList", path = "operationrepo")
public class Operation implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String label;
	
	private Double amount;
	
	private Date operationDate;

	@Enumerated(EnumType.STRING)
	private OperationType type;
	
}
